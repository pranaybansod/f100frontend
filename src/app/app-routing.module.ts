import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { COAComponent } from './coa/coa.component';
import { IndexComponent } from './index/index.component';
import { ProductDescComponent } from './product-desc/product-desc.component';
import { UserCoaComponent } from './user-coa/user-coa.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path:'user',component:UsersComponent},
  {path:'index',component:IndexComponent},
  {path:'productDesc',component:ProductDescComponent},
  {path:'category',component:CategoriesComponent},
  {path:'coa',component:COAComponent},
  {path:'productview',component:UserCoaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    useHash: true,
    scrollPositionRestoration: 'enabled', // Add options right here
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
