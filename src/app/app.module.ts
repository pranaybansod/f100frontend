import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { IndexComponent } from './index/index.component';
import { ProductDescComponent } from './product-desc/product-desc.component';
import { CategoriesComponent } from './categories/categories.component';
import { COAComponent } from './coa/coa.component';
import { MenuComponent } from './menu/menu.component';
import { UserCoaComponent } from './user-coa/user-coa.component';
import {Ng2TelInput, Ng2TelInputModule} from 'ng2-tel-input';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    IndexComponent,
    ProductDescComponent,
    CategoriesComponent,
    COAComponent,
    MenuComponent,
    UserCoaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    Ng2TelInputModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [
    Ng2TelInput
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
