// import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  prodList:any=[];
  cateData:any=this.service.cateData;
 cData:any=[];
  // prod: any;
  constructor(private service:ServicesService,private router:Router) { }

  ngOnInit(): void {
  
    console.log("cate  ",this.cateData)
      this.cateDetails();
  }

async cateDetails(){
  // let length=this.prodList;
  this.service.getProductDetails().subscribe((res:any)=>{
    this.prodList=res.results;
    this.prodList.forEach( (prod:any) => {
    
      if(this.cateData.id===prod.category){
        this.cData.push(prod);
      }
    })

});

  
}

linkProd(pdata:any){
  this.service.productDescription(pdata);
  this.router.navigateByUrl("/productDesc");
}


}
