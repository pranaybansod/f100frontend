// import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-coa',
  templateUrl: './coa.component.html',
  styleUrls: ['./coa.component.css']
})
export class COAComponent implements OnInit {

  constructor(private service:ServicesService,private router:Router) { }

  ngOnInit(): void {
    this.f100ProdList();
  }

f100:any=[];

f100ProdList(){
  this.service.getF100Prod().subscribe((res:any)=>{
    this.f100=res.results;
    console.log(this.f100)
  })
}


linkProdDesc(pdata:any){
  console.log("p ",pdata);
this.service.productDescription(pdata);
this.router.navigateByUrl("/productDesc");
}


product_view(list:any){
  this.service.getProductView(list.id);
  this.router.navigateByUrl("/productview");
}
}
