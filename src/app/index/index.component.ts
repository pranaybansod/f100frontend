import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GET_CATEGORY,GET_PRODUCT,SEARCH } from '../serverUrls'
import { Router } from '@angular/router';
import { ServicesService } from '../services/services.service';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  public cateList:any=[];
 public prodList:any=[];

 public evt1=false;
 public evt2=true;
evt!:boolean;

  constructor(private http:HttpClient,private router:Router,private service:ServicesService) { }

  ngOnInit(): void {
    this.loadCategory();
    this.loadProducts();
    this.evt=true;
    this.evt1=false;
    this.evt2=true;
  }

  public linkLogin(){
    this.evt1=false;
    this.evt2=true;
  }
public linkRegister(){
  this.evt2=false;
  this.evt1=true;
}   

login(ldata:any){
console.log(ldata);

}

register(rdata:any){
console.log(rdata);
}


searchdiv(){
  this.evt=false;
}

coa(){
  this.router.navigateByUrl('/coa');
}

loadCategory(){
  this.service.getCategoryDetails().subscribe((res:any)=>{
    console.log("responser",res);
    this.cateList=res;
  })
}

loadProducts(){
 this.service.getProductDetails().subscribe((res:any)=>{
  this.prodList=res.results;
  console.log("prod ",this.prodList);
  });
 
}
public searchData:any=[];
 search(sdata:any){
   
   console.log("sdata",sdata)
  //  let data=JSON.stringify(sdata);
  this.service.getSearchDetails(sdata).subscribe((res:any)=>{
    console.log("search ",res);
    this.searchData=res.results;
    // console.log("sd",SEARCH+`${sdata}`)
  })    

}

linkProdDesc(pdata:any){
  console.log("p ",pdata);
this.service.productDescription(pdata);
this.router.navigateByUrl("/productDesc");
}


linkCategory(cate:any){
this.service.categories(cate);
}

}
