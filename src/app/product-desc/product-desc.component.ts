import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
// import { threadId } from 'worker_threads';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-product-desc',
  templateUrl: './product-desc.component.html',
  styleUrls: ['./product-desc.component.css']
})
export class ProductDescComponent implements OnInit {

  constructor(private http:HttpClient,private service:ServicesService) { }
discount:any;

  ngOnInit(): void {
    this.prodDesc();
  }

prod:any;

prodDesc(){
      this.prod=this.service.prodData;
      let mrp=this.prod.mrp;
      let price=this.prod.price;
      this.discount=Math.ceil(((mrp-price)/mrp)*100);
      console.log(typeof(mrp),typeof(price))
      console.log(this.prod);
}

}
