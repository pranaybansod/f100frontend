import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { F100, GET_CATEGORY, GET_PRODUCT, PRODUCT_VIEW, SEARCH } from '../serverUrls';
// import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http:HttpClient) { }
// prodList:any=[];

getProductDetails(){

  return this.http.get(GET_PRODUCT);
}

getCategoryDetails(){
  return this.http.get(GET_CATEGORY);
}

getSearchDetails(sdata:any){
  return this.http.get(SEARCH+`${sdata}`)
}

prodData:any;

productDescription(pdata:any){
  this.prodData=pdata;

}

cateData:any;
categories(cate:any){
  this.cateData=cate;
}


getF100Prod(){
 return this.http.get(F100);
}


productView:any;
getProductView(pdata:any){
//  return this.http.get(PRODUCT_VIEW+`${pdata}/`);
this.productView=pdata;
}



}