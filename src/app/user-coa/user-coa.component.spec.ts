import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCoaComponent } from './user-coa.component';

describe('UserCoaComponent', () => {
  let component: UserCoaComponent;
  let fixture: ComponentFixture<UserCoaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserCoaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
