import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PRODUCT_VIEW } from '../serverUrls';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-user-coa',
  templateUrl: './user-coa.component.html',
  styleUrls: ['./user-coa.component.css']
})
export class UserCoaComponent implements OnInit {

  constructor(private service:ServicesService,private http:HttpClient) { }

  ngOnInit(): void {
    this.getProductBuyer();
  }

prodView=this.service.productView;
buyers:any=[];

getProductBuyer(){
  this.http.get(PRODUCT_VIEW+`${this.prodView}/`).subscribe((res:any)=>{
    console.log(res);
    this.buyers=res.buyer;
  })

}

}
