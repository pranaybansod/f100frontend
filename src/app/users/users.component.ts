// import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { USER_REGISTER ,USER_LOGIN} from '../serverUrls';
// import { ThrowStmt } from '@angular/compiler';
// import { Ng2TelInput } from 'ng2-tel-input';
import { ToastrService } from 'ngx-toastr';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import Validation from '../utils/validation';

declare var jQuery: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private http:HttpClient,private toastr: ToastrService,private formBuilder: FormBuilder) { }
  public evt1=false;
  public evt2=true;

  form!: FormGroup;
  submitted = false;

  ngOnInit(): void {
    this.evt1=false;
      this.evt2=true;
      // this.process()
      this.re_pass;


      this.form = this.formBuilder.group(
        {
          fname: ['', Validators.required],
          lname: ['', Validators.required],

          // username: [
          //   '',
          //   [
          //     Validators.required,
          //     Validators.minLength(6),
          //     Validators.maxLength(20)
          //   ]
          // ],
          phone: ['', [ Validators.required, Validators.minLength(5), Validators.maxLength(15)]],// Validators.pattern("^[0-9]*$"),

          email: ['', [Validators.required, Validators.email]],
          password: [
            '' ],
          confirmPassword: ['', Validators.required],
          // acceptTerms: [false, Validators.requiredTrue]
        },
        
     
        
        
      );
    }

    public linkLogin(){
      this.evt1=false;
      this.evt2=true;
    }
  public linkRegister(){
    this.evt2=false;
    this.evt1=true;
  }   


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
 
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

// login(ldata:any){
// console.log(ldata);
// let jsondata=JSON.stringify(ldata);
// let jsonParse=JSON.parse(jsondata);
// console.log(jsondata);
// this.http.post(USER_LOGIN,jsonParse).subscribe((res)=>{
//   console.log("log data  ",res);
// })
// }

cCode:any;
dCode:any;

onCountryChange(event:any)
{
  console.log(event.dialCode);
  console.log(event.name);
  console.log(event.iso2);
  this.cCode=event.iso2;
  this.dCode=event.dialCode;
}

re_pass="";

// register(rdata:any){
  

//     rdata.dial_code=+this.dCode,
//     rdata.country_code=this.cCode
  
//   console.log(rdata,this.re_pass);
// // let jsondata=JSON.parse(rdata);
// if(rdata.password===this.re_pass){
//   let jsondata=JSON.stringify(rdata);
//   let jsonParse=JSON.parse(jsondata);
//   console.log(jsondata);
  
//     this.http.post(USER_REGISTER,jsonParse).subscribe((res)=>{
//       console.log("reg data ",res);
//     })
// }else{
//    this.toastr.error("Enter correct passwords")
// }

// }


get f(): { [key: string]: AbstractControl } {
  // console.log("djds ",this.submitted,this.form.controls);
  Validation.match('password', 'confirmPassword')
  return this.form.controls;
}
onSubmit(): void {
  this.submitted = true;

   console.log("this form",this.form)
  if (this.form.invalid) {
    console.log("invald",this.form.value)
    return;    
  }

  console.log(this.form.value);
}

onReset(): void {
  this.submitted = false;
  this.form.reset();
}

}

