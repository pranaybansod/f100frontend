import { AbstractControl, ValidatorFn } from '@angular/forms';

export default class Validation {
  static match(controlName: any, checkControlName: any): ValidatorFn {
    return (controls: AbstractControl|null) => {
      const control = controls?.get(controlName);
      const checkControl = controls?.get(checkControlName);
console.log("control ",control,checkControl);
      if (checkControl?.errors && !checkControl?.errors?.matching) {
        return null;
      }

      if (control?.value !== checkControl?.value) {
        checkControl?.setErrors({ matching: true });
        return { matching: true };
      } else {
        return null;
      }
    };
  }
}
